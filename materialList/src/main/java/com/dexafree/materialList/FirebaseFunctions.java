package com.dexafree.materialList;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.BundleCompat;
import android.widget.TextView;
import android.widget.Toast;

import com.dexafree.materialList.card.CardProvider;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Observable;

/**
 * Created by dejes on 9/15/2016.
 */
public class FirebaseFunctions extends Observable {
    String bookID;
    Context mContext;
    String status;
    public FirebaseFunctions(Context context, String bookID){
        this.mContext = context;
        this.bookID = bookID;
    }
    public FirebaseFunctions(Context context){
        this.mContext = context;
    }

    public FirebaseAuth getUserInstance(){
        return FirebaseAuth.getInstance();
    }
    public FirebaseUser getCurrentUser(){
        return getUserInstance().getCurrentUser();
    }
    public boolean isUserLoggedin(){
        FirebaseUser user = getUserInstance().getCurrentUser();
        if(user != null){
            return true;
        }
        return false;
    }
    public DatabaseReference getDatabaseReference(){
        return FirebaseDatabase.getInstance().getReference();
    }

    public String getBookId(){
        return bookID;
    }

    public void setBookStatus(String statusName, String bookName, String bookAuthor,String image){
        Book book = new Book(bookID,bookName, "by: " + bookAuthor,statusName,image);
        DatabaseReference mDbRef = getDatabaseReference();
        mDbRef.child("Book")
                .child(getCurrentUser().getUid())
                .child(getBookId())
                .setValue(book);
    }

    public String getBookStatus(){
        final DatabaseReference mDbRef = getDatabaseReference();
        final String[] status = new String[1];
        mDbRef.child("Book")
                .child(getCurrentUser()
                .getUid())
                .child(getBookId())
                .child("status")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String data = dataSnapshot.getValue(String.class);
                        if (String.valueOf(data).equals("1")) {
                            status[0] = "Currently Reading";
                        } else if (String.valueOf(data).equals("2")) {
                            status[0] = "Want to Read";
                        } else if (String.valueOf(data).equals("3"))
                            status[0] = "Read";
                        else {
                            status[0] = "Set Status";
                        }
                        Toast.makeText(mContext,getCurrentUser().getUid()+"\n"
                                +getBookId()+"\n"
                                +status[0], Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


        return status[0];
    }

    public void sendAnalytics(String eventName, Bundle bundle){
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setMinimumSessionDuration(20000);
        mFirebaseAnalytics.logEvent(eventName, bundle);
    }
}
