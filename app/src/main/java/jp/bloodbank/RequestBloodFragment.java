package jp.bloodbank;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dd.processbutton.FlatButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JP on 5/31/2017.
 */

public class RequestBloodFragment extends android.support.v4.app.Fragment {

    //Button
    @BindView(R.id.btnRequest)
    FlatButton btnRequest;

    //Variables
    Activity activity;
    String name,location,bloodtype,contact,age,request;

    //Firebase
    private FirebaseClass firebaseClass;
    private DatabaseReference mRootRef;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private DatabaseReference mDatabase;

    public RequestBloodFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize(){
        activity = getActivity();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        new RetrieveUserData().execute();
    }

    class RetrieveUserData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Users")
                            .child(mAuth.getCurrentUser().getUid())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final UserClass user = dataSnapshot.getValue(UserClass.class);
                                     name = user.getName();
                                     location = user.getAddress();
                                     contact = user.getContact();
                                     age = user.getAge();
                                     bloodtype = user.getBloodtype();
                                     request = user.getRequest();

                                    if(request.equals("YES")){
                                        btnRequest.setText("Requested for Blood!");
                                        btnRequest.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                CancelRequest();
                                            }
                                        });

                                    }
                                    else{
                                        btnRequest.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                RequestBlood();
                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }

    public void RequestBlood(){
        new MaterialDialog.Builder(activity)
                .titleColorRes(R.color.colorPrimary)
                .contentColor(getResources().getColor(R.color.colorPrimary))
                .title("Choose Option")
                .content("Request for Blood?")
                .items(R.array.choice)
                .widgetColorRes(R.color.colorPrimary)
                .backgroundColorRes(R.color.colorAccent)
                .positiveColorRes(R.color.colorPrimaryDark)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if(which == 0){
                            BloodClass request = new BloodClass(mAuth.getCurrentUser().getUid(),name,location,contact,age,bloodtype);
                            mDatabase.child("Requests").child(mAuth.getCurrentUser().getUid()).setValue(request);
                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("request").setValue("YES");

                            btnRequest.setText("Requested for Blood!");
                        }
                        return true;
                    }
                })
                .positiveText("Ok")
                .cancelable(true)
                .show();
    }
    public void CancelRequest(){
        new MaterialDialog.Builder(activity)
                .titleColorRes(R.color.colorPrimary)
                .contentColor(getResources().getColor(R.color.colorPrimary))
                .title("Choose Option")
                .content("Cancel Request?")
                .items(R.array.choice)
                .widgetColorRes(R.color.colorPrimary)
                .backgroundColorRes(R.color.colorAccent)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if(which == 0){
                            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
                            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
                            mRootRef.child("Requests")
                                    .child(mAuth.getCurrentUser().getUid())
                                    .removeValue();
                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("request").setValue("NO");

                            btnRequest.setText("Request Now!");
                        }
                        return true;
                    }
                })
                .positiveText("Ok")
                .cancelable(true)
                .show();
    }

}
