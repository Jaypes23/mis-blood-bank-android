package jp.bloodbank;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dd.processbutton.FlatButton;
import com.dd.processbutton.iml.ActionProcessButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    //TextInputLayout
    @BindView(R.id.login_usernametextinput)
    TextInputLayout tilEmail;
    @BindView(R.id.login_passwordtextinput)
    TextInputLayout tilPassword;

    //EditText
    @BindView(R.id.login_username) EditText etUserId;
    @BindView(R.id.login_password) EditText etPassword;

    //Button
    @BindView(R.id.btnLogin)
    ActionProcessButton btnLogin;
    @BindView(R.id.btnSignUp)
    FlatButton btnRegister;

    //Firebase
    private FirebaseClass firebasefunctions;
    private DatabaseReference mRootRef;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        Initialize();
    }

    public void Initialize(){
        firebasefunctions = new FirebaseClass(this);
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mAuth = firebasefunctions.getUserInstance();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                startActivity(intent);
//                finish();
            }
        };
    }

    public void Login(View v) {
        btnLogin.setMode(ActionProcessButton.Mode.PROGRESS);
        btnLogin.setProgress(50);
        if (TextUtils.isEmpty(etUserId.getText().toString())) {
            tilEmail.setError("Enter email address");
            btnLogin.setProgress(0);
            return;
        }
        if (TextUtils.isEmpty(etPassword.getText().toString())) {
            tilPassword.setError("Enter password");
            btnLogin.setProgress(0);
            return;
        }
        if (!isValidEmail(etUserId.getText().toString())) {
            tilEmail.setError("Enter a valid email address");
            btnLogin.setProgress(0);
            return;
        } else {
            mAuth.signInWithEmailAndPassword(etUserId.getText().toString(), etPassword.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull final Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                btnLogin.setProgress(0);
                                try {
                                    throw task.getException();
                                } catch (FirebaseAuthInvalidUserException e) {
                                    new MaterialDialog.Builder(LoginActivity.this)
                                            .title("Login Failed")
                                            .content("User Not Found")
                                            .positiveText("Close")
                                            .show();
                                    etPassword.setText("");
                                } catch (FirebaseAuthInvalidCredentialsException e) {
                                    new MaterialDialog.Builder(LoginActivity.this)
                                            .title("Login Failed")
                                            .content("Email or Password did not match. Please try again")
                                            .positiveText("Close")
                                            .show();
                                    etPassword.setText("");
                                } catch (FirebaseNetworkException e) {
                                    new MaterialDialog.Builder(LoginActivity.this)
                                            .title("Login Failed")
                                            .content("Please Check Internet Connection, and try again")
                                            .positiveText("Close")
                                            .show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else {
                                btnLogin.setProgress(100);
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
        }
    }


    public boolean isValidEmail(CharSequence target) {
        return target != null && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
}
