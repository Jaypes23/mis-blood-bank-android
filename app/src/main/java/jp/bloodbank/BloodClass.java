package jp.bloodbank;

/**
 * Created by JP on 6/4/2017.
 */

/**
 * Created by JP on 5/29/2017.
 */

public class BloodClass {
    public String uId;
    public String name;
    public String bloodtype;
    public String location;
    public String age;
    public String contact;


    public BloodClass(){

    }

    public BloodClass(String uId,String name,String location,String contact, String age,String bloodtype){
        this.uId = uId;
        this.name = name;
        this.location = location;
        this.contact = contact;
        this.age = age;
        this.bloodtype = bloodtype;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBloodtype() {
        return bloodtype;
    }

    public void setBloodtype(String bloodtype) {
        this.bloodtype = bloodtype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

}
