package jp.bloodbank;

/**
 * Created by JP on 5/29/2017.
 */

public class UserClass {
    public String name;
    public String email;
    public String password;
    public String bloodtype;
    public String address;
    public String gender;
    public String contact;
    public String birthday;
    public String timestamp;
    public String age;
    public String request;
    public String firstLogin;


    public UserClass(){

    }

    public UserClass(String email, String password, String name, String birthday,
                     String age, String gender, String contact, String address, String bloodtype, String timestamp,String request){

        this.email = email;
        this.password = password;
        this.name = name;
        this.birthday = birthday;
        this.age = age;
        this.gender = gender;
        this.contact = contact;
        this.address = address;
        this.bloodtype = bloodtype;
        this.timestamp = timestamp;
        this.request = request;

    }


    public String getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(String firstLogin) {
        this.firstLogin = firstLogin;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBloodtype() {
        return bloodtype;
    }

    public void setBloodtype(String bloodtype) {
        this.bloodtype = bloodtype;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }
}
