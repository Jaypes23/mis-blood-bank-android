package jp.bloodbank;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JP on 4/15/2017.
 */

public class ProfileFragment extends android.support.v4.app.Fragment {

    //TextView
    @BindView(R.id.tvnameholder)
    TextView tvnamehldr;
    @BindView(R.id.tvemailhldr)
    TextView tvemailhldr;
    @BindView(R.id.tvaddresshldr)
    TextView tvaddresshldr;
    @BindView(R.id.tvcontacthldr)
    TextView tvcontacthldr;
    @BindView(R.id.tvbirthdayhldr)
    TextView tvbdayhldr;
    @BindView(R.id.tvtimehldr)
    TextView tvtimehldr;
    @BindView(R.id.tvBloodType)
    TextView tvBloodhldr;

    //Buttons
//    @BindView(R.id.btnEdit)
//    FancyButton btnUpdate;

    //Variables
    Fragment fragment = null;
    Activity activity;

    //MaterialDialog
    private MaterialDialog viewprofile;


    //Firebase
    private FirebaseClass firebaseClass;
    private DatabaseReference mRootRef;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_viewprofile, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize(){
        activity = getActivity();
        new RetrieveUserData().execute();

        viewprofile = new MaterialDialog.Builder(activity)
                .title("Loading Content")
                .content("Please wait...")
                .widgetColorRes(R.color.colorPrimaryDark)
                .progress(true, 0)
                .show();

//        btnUpdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Fragment fragment = null;
//                FragmentTransaction transaction = getFragmentManager().beginTransaction();
//                fragment = new UpdateProfileFragment();
//                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
//                transaction.replace(R.id.frame_container, fragment);
//                transaction.commit();
//            }
//        });
    }

    class RetrieveUserData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Users")
                            .child(mAuth.getCurrentUser().getUid())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final UserClass user = dataSnapshot.getValue(UserClass.class);
                                    String name = user.getName();
                                    String email = user.getEmail();
                                    String address = user.getAddress();
                                    String contact = user.getContact();
                                    String bday = user.getBirthday();
                                    String time = user.getTimestamp();
                                    String blood = user.getBloodtype();

                                    tvnamehldr.setText(name);
                                    tvemailhldr.setText(email);
                                    tvaddresshldr.setText(address);
                                    tvcontacthldr.setText(contact);
                                    tvbdayhldr.setText(bday);
                                    tvtimehldr.setText(time);
                                    tvBloodhldr.setText(blood);

                                    viewprofile.dismiss();

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }


}
