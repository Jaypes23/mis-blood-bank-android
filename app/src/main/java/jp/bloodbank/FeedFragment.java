package jp.bloodbank;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.listeners.RecyclerItemClickListener;
import com.dexafree.materialList.view.MaterialListView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by JP on 6/6/2017.
 */

public class FeedFragment extends android.support.v4.app.Fragment {

    //Variables
    Fragment fragment = null;
    Activity activity;

    @BindView(R.id.lv_newsfeed)
    MaterialListView lvfeed;

    //Firebase
    private FirebaseClass firebaseClass;
    private DatabaseReference mRootRef;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public FeedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    public void Initialize(){
        activity = getActivity();
        new RetrieveRequests().execute();
    }

    class RetrieveRequests extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            FirebaseClass firebaseFunctions = new FirebaseClass(activity);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Requests").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (final DataSnapshot dateSnapshot : dataSnapshot.getChildren()) {
                        mRootRef.child("Requests")
                                .child(dateSnapshot.getKey())
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        final BloodClass blood = dataSnapshot.getValue(BloodClass.class);
                                        final Card card = new Card.Builder(activity)
                                                .withProvider(new CardProvider())
                                                .setLayout(R.layout.bloodbanklayout)
                                                .setBloodName("Name: ")
                                                .setBloodNamehldr(String.valueOf(blood.getName()))
                                                .setBloodBankType("Blood Type: ")
                                                .setBloodBankTypehldr(String.valueOf(blood.getBloodtype()))
                                                .setBloodBankRequest("Location: ")
                                                .setBloodBankRequesthldr(String.valueOf(blood.getLocation()))
                                                .setBloodBankLocation("Contact: ")
                                                .setBloodBankLocationhldr(String.valueOf(blood.getContact()))
                                                .endConfig()
                                                .build();

                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                lvfeed.getAdapter().add(0, card);
                                                lvfeed.scrollToPosition(0);

                                                lvfeed.addOnItemTouchListener(new RecyclerItemClickListener.OnItemClickListener() {
                                                    @Override
                                                    public void onItemClick(@NonNull Card card, int position) {
                                                        final CircleImageView btnCall = (CircleImageView) activity.findViewById(R.id.call);
                                                        final CircleImageView btnText = (CircleImageView) activity.findViewById(R.id.text);

                                                        btnCall.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {
                                                                String phone = blood.getContact().trim();
                                                                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                                                                startActivity(intent);
                                                            }
                                                        });

                                                        btnText.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {
                                                                String number = blood.getContact().trim();  // The number on which you want to send SMS
                                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
                                                            }
                                                        });
                                                    }

                                                    @Override
                                                    public void onItemLongClick(@NonNull Card card, int position) {

                                                    }
                                                });
                                            }
                                        });
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }


                                });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

    }



}
