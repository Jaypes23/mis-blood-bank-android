package jp.bloodbank;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dd.processbutton.iml.ActionProcessButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by JP on 5/29/2017.
 */

public class RegisterActivity extends Activity{
    //TextInputLayout
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.til_name)
    TextInputLayout tilName;
    @BindView(R.id.til_password)
    TextInputLayout tilPassword;
    @BindView(R.id.til_address)
    TextInputLayout tilAddress;
    @BindView(R.id.til_contact)
    TextInputLayout tilContact;
    @BindView(R.id.til_birthday)
    TextInputLayout tilBirthday;
    @BindView(R.id.til_bloodtype)
    TextInputLayout tilBloodType;
    @BindView(R.id.til_gender)
    TextInputLayout tilGender;

    //EditText
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_contact)
    EditText etContact;
    @BindView(R.id.et_birthday)
    EditText etBirthday;
    @BindView(R.id.et_bloodtype)
    EditText etBloodtype;
    @BindView(R.id.et_gender)
    EditText etGender;

    //ActionProcessButton
    @BindView(R.id.btnRegister)
    ActionProcessButton btnRegister;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    private int year,month,day;
    private String email,password,name,bloodtype,contact,gender,address,age,timestamp;
    private SimpleDateFormat currentTimeStamp;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        Initialize();
    }

    public void Initialize() {
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        currentTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        timestamp = currentTimeStamp.format(new Date());

        etBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int Cyear,
                                                  int monthOfYear, int dayOfMonth) {
                                etBirthday.setText(/*"Birthday: " + */(monthOfYear + 1) + "/" + (dayOfMonth) + "/" + Cyear);
                            }
                        }, year, month, day);
                datePickerDialog.show();
            }
        });

        etGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChooseGender();
            }
        });

        etBloodtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChooseBloodType();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnRegister.setMode(ActionProcessButton.Mode.PROGRESS);
                btnRegister.setProgress(50);

                final String email = etEmail.getText().toString().trim();
                final String password = etPassword.getText().toString().trim();
                final String name = etName.getText().toString().trim();
                final String birthday = etBirthday.getText().toString().trim();
                final String gender = etGender.getText().toString().trim();
                final String bloodtype = etBloodtype.getText().toString().trim();
                final String address = etAddress.getText().toString().trim();
                final String contact = etContact.getText().toString().trim();

                if (TextUtils.isEmpty(etName.getText().toString())) {
                    etName.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etBirthday.getText().toString())) {
                    etBirthday.setError("Required");
                    return;
                }
                else{
                    //Get Age
                    String parser = etBirthday.getText().toString();
                    String delims = "[/]+";
                    String[] tokens = parser.split(delims);
                    int years = Integer.parseInt(tokens[2]);
                    int yearCalculate = Calendar.getInstance().get(Calendar.YEAR);
                    int agehldr = yearCalculate - years;
                    final String age = String.valueOf(agehldr);
                }
                if (TextUtils.isEmpty(etBloodtype.getText().toString())) {
                    etBloodtype.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etGender.getText().toString())) {
                    etGender.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etAddress.getText().toString())) {
                    etAddress.setError("Required");
                    return;
                }
                if (TextUtils.isEmpty(etContact.getText().toString())) {
                    etContact.setError("Required");
                    return;
                }
                if (etContact.length() < 11) {
                    etContact.setError("Contact number must be atleast 12 characters long");
                    return;
                }
                if (!isValidEmail(etEmail.getText().toString().trim())) {
                    etEmail.setError("Enter a valid Email Address");
                    return;
                }
                if (etPassword.length() < 6) {
                    etPassword.setError("Password must be atleast 6 characters long");
                    return;
                } else {
                    mAuth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        UserClass user = new UserClass(email, password, name, birthday, age, gender, contact, address, bloodtype, timestamp, "NO");
                                        mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).setValue(user);
                                        btnRegister.setProgress(100);
                                        Toast.makeText(RegisterActivity.this, "Sign Up Successful!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        try {
                                            throw task.getException();
                                        } catch (FirebaseAuthUserCollisionException e) {
                                            new MaterialDialog.Builder(RegisterActivity.this)
                                                    .title("Register Failed")
                                                    .content("Email already in use!")
                                                    .positiveText("Close")
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            Intent intent = new Intent(RegisterActivity.this, RegisterActivity.class);
                                                            startActivity(intent);
                                                            finish();
                                                        }
                                                    })
                                                    .show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                }
            }
        });
    }

    public void ChooseGender(){
        new MaterialDialog.Builder(RegisterActivity.this)
                .titleColorRes(R.color.colorPrimary)
                .contentColor(getResources().getColor(R.color.colorPrimary))
                .title("Choose Gender")
                .content("Choose One")
                .items(R.array.gender)
                .widgetColorRes(R.color.colorPrimary)
                .backgroundColorRes(R.color.colorAccent)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if(which == 0){
                         gender = "Male";
                         etGender.setText(gender);
                        }
                        if(which == 1){
                         gender = "Female";
                         etGender.setText(gender);
                        }
                        return true;
                    }
                })
                .cancelable(true)
                .positiveText("Continue")
                .show();
    }

    public void ChooseBloodType(){
        new MaterialDialog.Builder(RegisterActivity.this)
                .titleColorRes(R.color.colorPrimary)
                .contentColor(getResources().getColor(R.color.colorPrimary))
                .title("Choose Blood Type")
                .content("Choose One")
                .items(R.array.bloodtype)
                .widgetColorRes(R.color.colorPrimary)
                .backgroundColorRes(R.color.colorAccent)
                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if(which == 0){
                            bloodtype = "O-";
                            etBloodtype.setText(bloodtype);
                        }
                        if(which == 1){
                            bloodtype = "O+";
                            etBloodtype.setText(bloodtype);
                        }
                        if(which == 2){
                            bloodtype = "A-";
                            etBloodtype.setText(bloodtype);
                        }
                        if(which == 3){
                            bloodtype = "A+";
                            etBloodtype.setText(bloodtype);
                        }
                        if(which == 4){
                            bloodtype = "B-";
                            etBloodtype.setText(bloodtype);
                        }
                        if(which == 5){
                            bloodtype = "B+";
                            etBloodtype.setText(bloodtype);
                        }
                        if(which == 6){
                            bloodtype = "AB-";
                            etBloodtype.setText(bloodtype);
                        }
                        if(which == 7){
                            bloodtype = "AB+";
                            etBloodtype.setText(bloodtype);
                        }
                        return true;
                    }
                })
                .cancelable(true)
                .positiveText("Continue")
                .show();
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

}
